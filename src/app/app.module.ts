import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { ControlsComponent } from './components/controls/controls.component';
import { CitiesListComponent } from './components/cities-list/cities-list.component';
import { CityWeatherComponent } from './components/city-weather/city-weather.component';
import {LocationService} from './services/location.service';
import {HttpClientModule} from '@angular/common/http';
import {CitiesService} from './services/cities.service';
import { CitiesItemComponent } from './components/cities-list/cities-item/cities-item.component';
import {FormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    FooterComponent,
    ControlsComponent,
    CitiesListComponent,
    CityWeatherComponent,
    CitiesItemComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
  ],
  providers: [LocationService, CitiesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
