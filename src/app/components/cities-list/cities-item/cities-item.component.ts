import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-cities-item',
  templateUrl: './cities-item.component.html',
  styleUrls: ['./cities-item.component.scss']
})
export class CitiesItemComponent implements OnInit {
  @Input() name: string;
  @Input() country: string;

  constructor() { }

  ngOnInit() {
  }

}
