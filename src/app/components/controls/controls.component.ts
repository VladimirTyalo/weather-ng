import {Component, OnInit} from '@angular/core';

import {CitiesService} from '../../services/cities.service';
import {City} from '../../shared/interfaces';


@Component({
  selector: 'app-controls',
  templateUrl: './controls.component.html',
  styleUrls: ['./controls.component.scss']
})
export class ControlsComponent implements OnInit {
  search = '';
  cities: City[];

  constructor(private citiesService: CitiesService) {
  }

  ngOnInit() {
    this.citiesService.cities$.subscribe((cities: City[]) => {
      this.cities = cities;
    });
  }

  onChange(ev) {
    this.search = ev.target.value;
    this.citiesService.searchChanged$.next(this.search);
  }
}
