import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {LocationService} from '../../services/location.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  encapsulation: ViewEncapsulation.None

})
export class HeaderComponent implements OnInit {
  country = '';
  city = '';
  region = '';

  constructor(private locationService: LocationService) {
  }

  ngOnInit() {
    this.locationService.getCurrentLocation()
      .subscribe(
      (res) => {
       this.country = res.country;
       this.city = res.city;
       this.region = res.region;
      },
      (err) => console.log(err)
    );
  }
}
