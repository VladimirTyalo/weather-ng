import {CityLocation} from '../shared/interfaces';

export const cityLocation: CityLocation = {
  city: 'Kuibyschew',
  country: 'RU',
  hostname: '5x165x60x230.dynamic.samara.ertelecom.ru',
  ip: '5.165.60.230',
  loc: '53.2001,50.1500',
  org: 'AS34533 JSC ER-Telecom Holding',
  region: 'Samara',
};
