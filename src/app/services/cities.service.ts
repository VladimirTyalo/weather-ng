import qs from 'qs';
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {config} from '../app.config';
import {BehaviorSubject, Observable, EMPTY} from 'rxjs';
import {switchMap, debounceTime, takeUntil, skip, distinctUntilChanged, map} from 'rxjs/operators';
import {City} from '../shared/interfaces';

@Injectable({
  providedIn: 'root'
})
export class CitiesService {
  searchChanged$ = new BehaviorSubject('');
  cities$: Observable<City[]>;

  constructor(private http: HttpClient) {
    this.cities$ = this.searchChanged$.pipe(
      debounceTime(500),
      distinctUntilChanged(),
      switchMap(search =>
        this.requestCityList(search).pipe(
          takeUntil(this.searchChanged$.pipe(skip(1)))))
    );
  }

  private getQueryParams(partName: string) {
    const pattern = '^' + partName;
    const query = '{name:{$regex:"' + pattern + '",$options:"i"}}';
    const sort = '{name: 1}';
    const limit = 10;

    return {q: query, s: sort, l: limit, apiKey: config.mongodb.apiKey};
  }


  private requestCityList(partName: string): Observable<City[]> {
    if (!partName) {
      return EMPTY;
    }
    const urlParams = qs.stringify(this.getQueryParams(partName));

    return this.http.get(`${config.mongodb.url}?${urlParams}`)
      .pipe(map(res => {
          const cities: City[] = res as City[];
          return cities;
        })
      );
  }
}
