import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {config} from '../app.config';
import {CityLocation} from '../shared/interfaces';


@Injectable()
export class LocationService {
  constructor(private http: HttpClient) {
  }

  getCurrentLocation(): Observable<CityLocation> {
    return this.http.get(config.ipinfo.host).pipe(
      map(res => {
        const cityLocation: CityLocation = res as CityLocation;
        return cityLocation;
      })
    );
  }
}
