export interface CityLocation  {
  city: string;
  country: string;
  hostname: string;
  ip: string;
  loc: string;
  org: string;
  region: string;
}

interface Coord {
  lat: number;
  lon: number;
}

export interface City {
  coord: Coord;
  country: string;
  'country-name': string;
  id: number;
  name: string;
}
